#!/bin/bash
<<'README'
Toolkit Project en BASH
Este proyecto es un trabajo para la Universidad Nacional Del Comahue, 
sus integrantes son: 
			o Rodrigo Ochoa
			o Daniel Mercado
			o Felipe Cantisani

Este es un proyecto de software libre y por lo tanto no nos responsabilizamos 
de los daños que puedan ocurrir. El empleo de este script puede matar a su 
gato. Usted está advertido!

           .'\   /`.
         .'.-.`-'.-.`.
    ..._:   .-. .-.   :_...
  .'    '-.(o ) (o ).-'    `.
 :  _    _ _`~(_)~`_ _    _  :
:  /:   ' .-=_   _=-. `   ;\  :
:   :|-.._  '     `  _..-|:   :
 :   `:| |`:-:-.-:-:'| |:'   :
  `.   `.| | | | | | |.'   .'
    `.   `-:_| | |_:-'   .'
      `-._   ````    _.-'
          ``-------''
Art by Joan Stark
README

ruta="$(dirname $(realpath $0))"

declare -i seleccion_ppal=0
declare bandera_ppal=true

# source de librerias 
source "$ruta""/libUsuarios.sh"
source "$ruta""/libDiscos.sh"
source "$ruta""/libInterface.sh"

while $bandera_ppal ; 
do
	echo "-----------------------------------------"
	echo "|            MENU PRINCIPAL             |"
	echo "-----------------------------------------"
	echo ""
	echo "1- Gestion de usuarios."
	echo "2- Gestion de discos."
	echo "3- Gestion de procesos."
	echo "4- Gestion de la memoria."
	echo "5- Gestion de texto."
	echo "6- Help."
	echo "9- Salir."
	
	read -p "#> " seleccion_ppal 
	case $seleccion_ppal in
		1) menuUsuarios ;;
		2) source "$ruta""/gestionDiscos.sh" ;;
		3) source "$ruta""/libProcesos.sh" ;;
		4) source "$ruta""/libMemoria.sh" ;;
		5) source "$ruta""/gestionTexto.sh" ;;
		6) # "Vamos al script de help."
			echo "NO IMPLEMENTADO"
			sleep 3 ;;
		9) echo "Adios!."
			sleep 1
			bandera_ppal=false ;;
		*) echo "ERROR: Opción incorrecta!"
			sleep 1 ;; 
	esac		
clear 
done

