#!/bin/bash


ramusage=$(free | awk '/Mem/{printf("Uso de memoria RAM: %.2f\n"), $3/$2*100}'| awk '{print $3}')

if [ "$ramusage" > 20 ]; then
	SUBJECT="ATENCIÓN: El uso de memoria es alto en $(hostname) at $(date)"
	MESSAGE="/tmp/Mail.out"
	TO="usuario@hostname.org"

	echo "La cantidad de memoria en uso es: $ramusage%" >> $MESSAGE
	echo "" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "Consumo de memoria con información de TOP" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "$(top -b -o +%MEM | head -n 20)" >> $MESSAGE
	echo "" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "Consumo de memoria con información de PS" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "$(ps -eo pid,ppid,%mem,%cpu,cmd --sort=-%mem | head)" >> $MESSAGE
	mail -s "$SUBJECT" "$TO" < $MESSAGE
	rm /tmp/Mail.out
fi
