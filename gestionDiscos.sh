#!/bin/bash
ruta="$(dirname $(realpath $0))"
source $ruta/funcionDiscos.sh
declare -i seleccion
declare bandera=true


while $bandera 
do
	#clear
	echo "-----------------------------------------"
	echo "|          GESTION DE DISCOS            |"
	echo "-----------------------------------------"
	echo ""
	echo "1- Reporte de Informacion de espacio en disco para cada servidor."
	echo "2- Reporte de rendimiento de los discos locales."
	echo "3- Reporte de actividad de I/O en los discos primarios de cada servidor."
	echo "4- Help."
	echo "9- Salir."
	echo -n "> "
	read seleccion
	case $seleccion in
		1) espacioDiscos ;;
		2) rendimiento;;
		3) actividadIO;;
		4) echo "Vamos al script de help.";;
		9) echo "Adios!."
			bandera=false ;;
		*) echo "Se ha ingresado un comando no válido. Vuelva a introducirlo"
			sleep 3 ;;
	esac		
done

