#!/bin/bash

<< 'Comentario'
En este script se encuentra el menu de Gestion de Procesos,
tambien se encuentran implementadas las funciones invocadas 
en el menu.
Comentario


declare -i seleccion
declare bandera=true

function verificaNumero {
<< 'Comentario'
Este metodo verifica que el numero ingresado sea valido, 
retorna el valor valido.
Comentario
verifica=true
while $verifica ; do
	printf "%s\n" "Ingrese un numero: "
	read numero
	if [ $numero  -lt 1 ] ; then
		printf "%s\n" "El valor ingresado es invalido! "
	else
		verifica=false
	fi
done
return $numero
}

function mayorConsumo {
<< 'Comentario'
Este metodo muestra los 5 procesos 
que consumen mas memoria RAM
Comentario

	printf "%s\n" "Los cinco procesos que mas memoria ram consumen son:"
	printf "\n"
	printf "%s\n" "USER %MEM COMMAND"
	ps aux --sort=%mem | tac | head -n 5 | awk '{print $1,$4,$11}'
	printf "\n"
	read -p "[Enter para continuar]"
	clear
}

function alertaProcesos {
<< 'Comentario'
Este metodo reporta una alerta si el numero 
de procesos es superior a un numero 
ingresado por el usuario.
Comentario
verificaNumero
echo "El valor es: $numero"
cantProcesos=$(ps aux | wc -l)
if [ "$numero" -lt "$cantProcesos" ] ;
then 
	printf "%s\n" "Alerta, la cantidad de procesos supera el umbral provisto!"	
fi
read -p "[Enter para continuar]"
clear
}

function seguimientoProc {
<< 'Comentario'
Este metodo realiza un seguimiento de un 
proceso especifico, realiza 10 iteraciones.
Comentario
printf "%s\n" "A continuacion se muestra una lista de los procesos activos(presione q para salir): "
sleep 3
ps -aux | awk '{print $2 " " $NF}' | less
sleep 1
verificaNumero
verifica=true
	while $verifica ; do
		valor=$(ps -up $numero)
		if [ $(echo $?) -eq 0 ] ; then
#			printf "%s\n" "A continuacion se muestra el seguimiento del proceso $numero en diez iteraciones: "
			read -p "Ingrese la cantidad de iteraciones que desea realizar:" cant
			printf "%s\n" "Iteracion USER	        PID %CPU %MEM  VSZ   RSS  TTY     STAT  START  TIME  COMMAND "
			for ((i=1;i<=$cant;i++)){
				valor=$(ps -up $numero | sed -n '2p') 
		        	echo "[$i]	  $valor"	
				sleep 2
			}
			verifica=false
		else
			printf "%s\n" "No existe el PID ingresado!"
			printf "%s\n" "Desea continuar? si/no" 
			read continua
			continua=$(echo $continua | tr a-z A-Z)
			if [ $continua = "NO" ] ; then
				verifica=false
				printf "%s\n" "Fin"
			else
				break
			fi
		fi
	done
	read -p "[Enter para continuar]"
	clear
}
function eliminaProc {
<< 'Comentario'
Este metodo realiza la eliminacion 
de un proceso(PID) especificado por el usuario
Comentario
verificaNumero
tipoSalida=$(ps -p $numero)		
if [ $? -eq 0 ] ; then
	ctrlIteraciones=0
	printf "%s\n" "El proceso $numero va a ser eliminado!"
	kill -15 $numero
	if [ $? -eq 0 ] ; then
		printf "%s\n" "El proceso $numero fue eliminado!"
	else
		#Ingresa en esta parte si no puedo eliminar
		#el proceso en el primer intento.
		while [ $ctrlIteraciones -lt 3 ] ; 
		do
			kill -15 $numero
			if [ $? -eq 0 ] ; then
				ctrlIteraciones=7
			else
				ctrlIteraciones=$ctrlIteraciones+1
			fi
		done
		if [ $ctrlIteraciones -lt 7 ] ; then
			printf "%s\n" "Advertencia, el proceso no ha podido 
			ser eliminado!!"
		else
			printf "%s\n" "El proceso $numero fue eliminado!"
		fi
	fi
else
	printf "%s\n" "El proceso $numero no existe!"
fi
}

function eliminarListaProc {
<< 'Comentario'
Este metodo elimina una lista de procesos 
ingresada por parametro.
Comentario
	declare bandera=true
	while $bandera ; do
		printf "%s\n" "Introducir el archivo con la lista de procesos: "
		read -p "#> " listaProcesos
		listaProcesos=$(readlink -f $listaProcesos)
		if [[ -f $listaProcesos ]] ; then
			printf "%s\n" "Archivo encontrado."
			bandera=false
			sleep 1
		else
			printf "%s\n" "Error: Archivo no encontrado!"
			read -p "Desea continuar? si/no" continua
			continua=$(echo $continua | tr a-z A-Z)
			if [ $continua = "NO" ] ; then
				bandera=false
			fi
		fi
	done
	lista=$(cat $listaProcesos)
	for numero in $lista
	do
		tipoSalida=$(ps -p $numero)		
		if [ $? -eq 0 ] ; then
			kill -15 $numero
			if [ $? -eq 0 ] ; then
				printf "%s\n" "El proceso $numero fue eliminado!"
			else
				kill -15 $numero
				if [ $? -eq 0 ] ; then
					printf "%s\n" "El proceso fue eliminado con exito!"
				else
					kill -9 $numero
					if [ $? -eq 0 ] ; then
						print "%s\n" "El proceso fue eliminado con mayor impetu!"
					else
						printf "%s\n" "Advertencia, el proceso no ha podido ser eliminado!"
					fi
				fi
			fi
		else
			printf "%s\n" "El proceso $numero no existe!"
		fi
	done
}


while $bandera 
do
	printf "%s\n" "-----------------------------------------"
	printf "%s\n" "|          GESTION DE PROCESOS          |"
	printf "%s\n" "-----------------------------------------"
	printf "\n"
	printf "%s\n" "1- Mayor consumo de memoria."
	printf "%s\n" "2- Crear una alerta de numero de procesos."
	printf "%s\n" "3- Monitorear un proceso especifico iuiuiuiui."
	printf "%s\n" "4- Eliminar un proceso especifico."
	printf "%s\n" "5- Eliminar una lista de procesos."
	printf "%s\n" "6- Help."
	printf "%s\n" "9- Salir."
	printf "%s" "> "
	read seleccion
	case $seleccion in
		1) mayorConsumo ;;
		2) alertaProcesos ;;
		3) seguimientoProc ;;
		4) eliminaProc ;;
		5) eliminarListaProc;;
		6) printf "%s\n" "Vamos al script de help.";;
		9) printf "%s\n" "Adios!."
			bandera=false ;;
		*) printf "%s\n" "Se ha ingresado un comando no válido. Vuelva a introducirlo"
			sleep 3 ;;
	esac
done

