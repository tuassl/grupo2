#!/bin/bash

# Bloque de variables

declare -i num=5
declare -i num2=9


# Bloque para funciones
function menu {
	
	while [ $num -ne $num2 ]; 
	do
		echo "-------------------------------------------------";
		echo "		MANUAL DE AYUDA 	      	       ";
		echo "-------------------------------------------------";
		echo "";
		echo "1- Altas de usuario.";
		echo "2- Cruce de llaves.";
		echo "3- Baja de usuarios.";
		echo "4- Reportes de usuarios del sistema.";
		echo "5- Reporte de usuarios activos.";
		echo "6- Help.";
		echo "9- Salir.";
		read num;
		case $num in
			1) altasDeUsuario ;;
			2) cruceDeLlaves ;;
			3) bajaDeUsuarios ;;
			4) usuariosDelSistema ;;
			5) usuariosActivos ;;
			6) echo "Vamos al script de help.";;
			9) echo "Adios!.";;
			*) echo "La opcion ingresada es incorrecta! ";;
		esac
		done
}

# Programa principal

menu



