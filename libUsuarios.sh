
function altasDeUsuarios {

	local retorno

	if [[ -f $1 ]] ; then 
		for linea in $(cat $1) ; do
			usuario=$(echo $linea | awk -F":" '{print $1}')
			contrasena=$(echo $linea | awk -F":" '{print $4}')
			sudo echo "Creando usuario $usuario con passwd: $contrasena"
			#sudo useradd -m -s /bin/bash -p $contrasena $usuario
		done
		# OK
		retorno=0
	else
		# Error
		retorno=1
	fi
	return $retorno
}

function bajasDeUsuarios {

	local retorno

	if [[ -f $1 ]] ; then 
		for linea in $(cat $1) ; do
			usuario=$(echo $linea | awk -F":" '{print $1}')
			sudo echo "Bloqueando usuario $usuario "
			#sudo usermod -e 1 -L $usuario
		done
		# OK
		retorno=0
	else
		# Error
		retorno=1
	fi
	return $retorno
}

function usuariosDelSistema {

	echo "# Nombres de usuarios de sistema"
	awk -F: '{ if ($3 < 1000) 
			print $1 
		}' /etc/passwd
	return $?
}

function usuariosActivos {

	echo "# Nombres de usuarios activos"
	awk -F: '{ 
		if ($3 >999 && $3 < 1500 && $7 !~ /nologin/) 
			print $1 
		}' /etc/passwd
	return $?
}

function llaveSsh {

	local retorno

	if [[ -f $1 ]] ; then 

		for linea in $(cat $1) ; do
			usuario=$(echo $linea | awk -F":" '{print $1}')
			ipMachine=$(echo $linea | awk -F":" '{print $2}')
			echo "Estamos copiando la llave con el usuario $usuario a la pc $ipMachine"
			#ssh-copy-id -n "$usuario""@""$ipMachine"
		done

		retorno=0
	else
		retorno=1
	fi

	return $retorno
}

function menuUsuarios {

	local bandera=true

	while $bandera
	do
		clear
		echo "-----------------------------------------"
		echo "|          GESTION DE USUARIOS          |"
		echo "-----------------------------------------"
		echo ""
		echo "1- Altas de usuario."
		echo "2- Cruce de llaves."
		echo "3- Baja de usuarios."
		echo "4- Reportes de usuarios del sistema."
		echo "5- Reporte de usuarios activos."
		#echo "6- Help."
		echo "9- Salir."
		read -p "#> " seleccion
		case $seleccion in
			1) 
				local bandera1=true

				while $bandera1 ; do
					echo "Introducir archivo csv de usuarios"
					read -p "#> " nombreNomina
					nombreNomina=$(readlink -f $nombreNomina)
					
					altasDeUsuarios $nombreNomina

					if [[ $? -eq 0 ]] ; then 
						echo "Finaliz� correctamente"
						continua1="no"
						echo ""
						read -p "[ENTER para continuar]"
					else
						echo "Error: ruta incorrecta"
						read -p "Desea continuar? [si/no]: " continua1
					fi

					#ver el tema de convertir a mayuscula o minuscula
					if [ "$continua1" == "no" -o "$continua1" == "NO"  ] ; then 
						bandera1=false
					fi
				done
				;;

			2) 
				local continua2=""
				
				while [ "$continua2" != "no" ] ; do
					echo "Introducir archivo csv de usuarios/ip-maquina"
					read -p "#> " nombreNomina
					nombreNomina=$(readlink -f $nombreNomina)

					llaveSsh $nombreNomina

					if [[ $? -eq 0 ]] ; then 
						echo "Finalizo correctamente"
						continua2="no"
						echo ""
						read -p "[ENTER para continuar]"
					else
						echo "Error: ruta incorrecta"
						read -p "Desea continuar? [si/no]: " continua2
					fi
				done 
				;;

			3) 
				local bandera3=true

				while $bandera3 ; do
					echo "Introducir archivo csv de usuarios"
					read -p "#> " nombreNomina
					nombreNomina=$(readlink -f $nombreNomina)
					
					bajasDeUsuarios $nombreNomina

					if [[ $? -eq 0 ]] ; then 
						echo "Finaliz� correctamente"
						continua3="no"
						echo ""
						read -p "[ENTER para continuar]"
					else
						echo "Error: ruta incorrecta"
						read -p "Desea continuar? [si/no]: " continua3
					fi

					#ver el tema de convertir a mayuscula o minuscula
					if [ "$continua3" == "no" -o "$continua3" == "NO"  ] ; then 
						bandera3=false
					fi
				done
				;;

			4) 
				usuariosDelSistema
				echo ""
				read -p "[ENTER para continuar]" ;;

			5) 
				usuariosActivos
				echo ""
				read -p "[ENTER para continuar]" ;;

			#6) echo "Vamos al script de help." ;;

			9) echo "Adios."
				sleep 1
				bandera=false ;;
			*) echo "ERROR: Opci�n incorrecta!"
				sleep 1 ;;
		esac		
	done
}
