#!/bin/bash

<< 'Comentario'
En este script se encuentra el menu de Gestion de Procesos,
tambien se encuentran implementadas las funciones invocadas 
en el menu.
Comentario


declare -i seleccion
declare bandera=true

function mayorConsumo {
<< 'Comentario'
Este metodo muestra los 5 procesos 
que consumen mas memoria RAM
Comentario

printf "%s\n" "Los cinco procesos que mas memoria ram consumen son:"
printf "\n"
printf "%s\n" "USER %MEM COMMAND"
ps aux --sort=%mem | tac | head -n 5 | awk '{print $1,$4,$11}'
printf "\n"
read -p "[Enter para continuar]"
clear
}

function alertaProcesos {
<< 'Comentario'
Este metodo reporta una alerta si el numero 
de procesos es superior a un numero 
ingresado por el usuario.
Comentario
verifica=true
while $verifica ; do
	printf "%s\n" "Ingrese una cantidad maxima de procesos: "
	read numero
	if [ $numero -lt 1 ] ;
	then
		printf "%s\n" "El valor ingresado es invalido! "
	else
		verifica=false
	fi
done
cantProcesos=$(ps aux | wc -l)
if [ "$numero" -lt "$cantProcesos" ] ;
then 
	printf "%s\n" "Alerta, la cantidad de procesos supera el umbral provisto!"	
fi
read -p "[Enter para continuar]"
clear
}

function seguimientoProc {
<< 'Comentario'
Este metodo realiza un seguimiento de un 
proceso especifico, realiza 10 iteraciones.
Comentario
verifica=true
while $verifica ; do
	printf "%s\n" "Ingrese el PID del proceso que desea analizar:"
	read num
	if [ $num -lt 1 -o $num -gt $(ps aux | wc -l) ] ;
	then
		printf "%s\n" "El valor ingresado es incorrecto!"
	else
		verifica=false
	fi
done
#ps aux --sort=%mem | tac | head -n 5 | awk '{print $1,$4,$11}'
printf "%s\n" "USER	     PID %CPU %MEM    VSZ   RSS  TTY     STAT START  TIME  COMMAND"
for ((i=1;i<11;i++)){
	#ps aux | sed -n '/"$(num)"/p'
	ps -up $num | sed -n '2p'
	sleep 2
}
read -p "[Enter para continuar]"
clear
}
function eliminaProc {
<< 'Comentario'
Este metodo realiza la eliminacion 
de un proceso(PID) especificado por el usuario
Comentario
verifica=true
while $verifica ; do
	printf "%s\n" "Ingrese el PID del proceso que desea eliminar:"
	read num
	if [ $num -lt 1 ] ;
	then
		printf "%s\n" "El valor ingresado es incorrecto!"
	else
		verificaa=false
	fi
done
bandera=true
#while $bandera ; do
#	resultado= <2 (kill -15 $num)
#	if [ [kill -15 $num] ==  ] ; then

#	fi
#done
#kill -15 $num
}

while $bandera 
do
	#clear
	printf "%s\n" "-----------------------------------------"
	printf "%s\n" "|          GESTION DE PROCESOS          |"
	printf "%s\n" "-----------------------------------------"
	printf "\n"
	printf "%s\n" "1- Mayor consumo de memoria."
	printf "%s\n" "2- Crear una alerta de numero de procesos."
	printf "%s\n" "3- Monitorear un proceso especifico."
	printf "%s\n" "4- Eliminar un proceso especifico."
	printf "%s\n" "5- Eliminar una lista de procesos."
	printf "%s\n" "6- Help."
	printf "%s\n" "9- Salir."
	printf "%s" "> "
	read seleccion
	case $seleccion in
		1) mayorConsumo ;;
		2) alertaProcesos ;;
		3) seguimientoProc ;;
		4) eliminaProc ;;
		5) printf "%s\n" "Vamos al script de eliminar una lista de procesos.";;
		6) printf "%s\n" "Vamos al script de help.";;
		9) printf "%s\n" "Adios!."
			bandera=false ;;
		*) printf "%s\n" "Se ha ingresado un comando no válido. Vuelva a introducirlo"
			sleep 3 ;;
	esac
done

