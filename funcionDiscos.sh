#!/bin/bash


function espacioDisco()   {

	if [[ -f $1 ]] ; then 
echo          "---------------------------"		
echo -e "\e[34mReporte de estado de Discos\e[0m"
echo          "---------------------------"
		for linea in $(cat $1) ; do
       			ipMaquina=$(echo $linea | awk -F":" '{print $2}')
       			nomUsuario=$(echo $linea | awk -F":" '{print $1}')
			echo "Informe de disco de la maquina $ipMaquina"
   			ssh $nomUsuario@$ipMaquina  df -h | grep /dev/ | sed '/tmpfs/d' | awk '{print echo "Dispositivo: " $1, 
    			echo "Espacio Total: " $2,
			echo "Disponible: " $3 *100 / $2 "%",
			echo "En uso: " $5,
			echo "Montado en: " $6
	                                                  }'| column -t 

		done

		retorno=0
	else
		retorno=1
	fi

	return $retorno
}





rendimiento(){ 
 which iostat 2>&1>/dev/null 
 existe=$?
 if [[ $existe -ne 0 ]] ;
	then
		echo "No existe el paquete systat"
	else
		
echo          "---------------------"		
echo -e "\e[34mRendimiento de Disco\e[0m"
echo          "---------------------"	

	iostat -dh 
	sleep 5
 fi
             }



actividadIO()
{	
		
echo          "-----------------------------------"		
echo -e "\e[34mActividad en timepo real de Discos\e[0m"
echo          "-----------------------------------"	

	sudo iotop -ob -n 2
	sleep 5
}

function espacioDiscos {
<< 'Comentario'
Listar espacio de discos y porcentajes.-
Comentario
declare bandera=true
while $bandera ; do
	printf "%s\n" "Introducir el archivo con la lista ip de hosts: "
	read -p "#> " listaMaquinas
	listaMaquinas=$(readlink -f $listaMaquinas)
        espacioDisco $listaMaquinas

       	if [[ $? -eq 0 ]]; then
		printf "%s\n" "Tarea Completa"
		bandera=false
	else
		printf "%s\n" "Error: Archivo no encontrado!"
		read -p "Desea continuar? si/no: " continua
		continua=$(echo $continua | tr a-z A-Z)
		if [ $continua = "NO" ] ; then
			bandera=false
		fi
	fi
done
	if [[ -f $1 ]] ; then 

		for linea in $(cat $1) ; do
			ipMaquina=$(echo $linea | awk -F":" '{print $2}')
			echo "Informe de disco de la maquina $ipMaquina"
			
		done

		retorno=0
	else
		retorno=1
	fi

	return $retorno
}


