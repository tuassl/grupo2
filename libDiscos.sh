#!/bin/bash

function seleccionDiscos {
	local retorno=0

	declare -i seleccion
	declare bandera=true

	while $bandera 
	do
		clear
		echo "-----------------------------------------"
		echo "|          GESTION DE DISCOS            |"
		echo "-----------------------------------------"
		echo ""
		echo "1- Reporte de Informacion de espacio en disco para cada servidor."
		echo "2- Reporte de rendimiento de los discos locales."
		echo "3- Reporte de actividad de I/O en los discos primarios de cada servidor."
		echo "4- Help."
		echo "9- Salir."
		echo -n "> "
		read seleccion
	done



	return $retorno
}

function menuDiscos {

	local retorno=0

	case $1 in
		1) echo "Vamos al script de reporte de info.";;
		2) echo "Vamos al script de rendimiento de discos.";;
		3) echo "Vamos al script de actividad de I/O";;
		4) echo "Vamos al script de help.";;
		9) echo "Adios!."
			retorno=1 ;;
		*) echo "Se ha ingresado un comando no válido. Vuelva a introducirlo"
			sleep 3 ;;
	esac		

	return $retorno

}
