#!/bin/bash

declare -i seleccion
declare bandera=true

while $bandera
do
	clear
	echo "-----------------------------------------"
	echo "|          GESTION DE TEXTOS            |"
	echo "-----------------------------------------"
	echo ""
	echo "1- Verificar el hostname."
	echo "2- Listado de servidores DNS."
	echo "3- Help."
	echo "9- Salir."
	echo -n "> "
	read seleccion
	case $seleccion in
		1) echo "Vamos al script para verificar el hostname.";;
		2) echo "Vamos al script para listar los servidores DNS.";;
		3) echo "Vamos al script de help.";;
		9) echo "Adios!."
			bandera=false ;;
		*) echo "Se ha ingresado un comando no válido. Vuelva a introducirlo"
			sleep 3 ;;
	esac		
done

